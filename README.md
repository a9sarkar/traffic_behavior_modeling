# Game theoretic models of human driving behavior.
This project creates models of human driving behavior from naturalistic data based on concepts from behavioral game theory. Details of the models and approaches are described [here](https://arxiv.org/abs/2009.10033) .


# Files

The dataset is now part of Waterloo Multi-Agent (WMA) Traffic Dataset available here: http://wiselab.uwaterloo.ca/waterloo-multi-agent-traffic-dataset/  (Intersection dataset)

Download the full dataset (3928 agents: 3649 vehicles, 264 pedestrians, 15 bicycles) [here](http://wiselab.uwaterloo.ca/multi-agent-intersection-traffic/dataset/intersection_dbfiles.zip) and the annotated videos [here](http://wiselab.uwaterloo.ca/multi-agent-intersection-traffic/annotated_videos/annotated_videos.zip).

The dataset is split into 14 SQLite files and corresponding annotated video files with file-ids : 769, 770, 771, 775, 776, 777, 778, 779, 780, 781, 782,  783, 784, 785. Each of these files corresponding to approximately 5 minutes of data recorded with an overhead drone over a busy intersection.


The database files contain details of all the agents, their trajectories, the map of the intersection, and various other metadata.  Details about the database schema along with instructions to create the models can be found  [here](https://arxiv.org/abs/2009.10033) . Feel free to send an email to atrisha.sarkar at uwaterloo.ca for any questions on the dataset or the code.

![basic scene](https://drive.google.com/uc?export=view&id=1F5-QT4VwoTFFFmCiZzJpwbPBl52wTDOC)

![lanes](https://drive.google.com/uc?export=view&id=1rF-HSP3P9iyrqXqAUKAZuIGl1zDc0UJ3)

![gates](https://drive.google.com/uc?export=view&id=18EGCzpvDwP-hZtzM4VWvL4Vs1WjXQXHp)
