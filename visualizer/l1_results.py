'''
Created on Nov 27, 2020

@author: Atrisha
'''
import numpy as np
import sqlite3
from all_utils import utils
import ast
import sys
import constants
from scipy import stats
import matplotlib.pyplot as plt
import json
import os
import os.path
import csv
import itertools
from equilibria import equilibria_core
from pylatex import Document, Section, Subsection, Tabular, MultiRow
import pylatex.utils
from collections import OrderedDict
import matplotlib as mpl

log = constants.common_logger

    
def get_newname(old_name):
    newname = []
    if old_name[0:2] == 'L1':
        newname.append('Ql1')
    elif old_name[0:4] == 'NASH':
        newname.append('PNE_QE')
    else:
        newname.append('Ql0')
    old_name = old_name.split('|')
    if newname[0] == 'Ql1':
        if old_name[0][2:] == 'BR':
            newname.append('MX')
        else:
            newname.append('MM')
    elif newname[0] == 'Ql0':
        if old_name[0][0:2] == 'BR':
            newname.append('MX')
        else:
            newname.append('MM')
    if old_name[1] != 'BASELINE_ONLY':
        if old_name[1][0:2] == 'BR':
            newname.append('MX')
        else:
            newname.append('MM')
        if old_name[-1] == 'GAUSSIAN':
            newname.append('S(1+G)')
        else:
            newname.append('S(1+B)')
               
    else:
        newname.append('S(1)')
    return ' '.join(newname)
        
     

def plot_util_hist():
    old_results_dir = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'results'))
    all_models,sit_distr = OrderedDict(), dict()
    files = [f for f in os.listdir(old_results_dir) if 'csv' in f]
    headers = ['SEGMENT','TASK','NEXT_CHANGE','SPEED','PEDESTRIAN','RELEV_VEHICLE','AGGRESSIVE']
    for f in files:
        print(f)
        with open(os.path.join(old_results_dir,f), newline='\n') as csvfile:
            datareader = csv.DictReader(csvfile)
            for row in datareader:
                if get_newname(row['EQ_TYPE']) not in all_models:
                    all_models[get_newname(row['EQ_TYPE'])] = []
                all_models[get_newname(row['EQ_TYPE'])].append(float(row['ON_EQ']))
                for h in headers:
                    if h not in sit_distr:
                        sit_distr[h] = dict()
                    if row[h] not in sit_distr[h]: 
                        sit_distr[h][row[h]] = dict()
                    if get_newname(row['EQ_TYPE']) not in sit_distr[h][row[h]]:
                        sit_distr[h][row[h]][get_newname(row['EQ_TYPE'])] = []
                    sit_distr[h][row[h]][get_newname(row['EQ_TYPE'])].append(float(row['ON_EQ']))
                
    all_models = OrderedDict(sorted(all_models.items(), key=lambda x:x[0], reverse=True))            
    print('*****MLE Estimates*****')
            
            
    mle_estimates = dict()
    for k,v in all_models.items():
        mle_estimates[k] = len(v)/sum(v)
        #mle_estimates[k] = stat_utils.fit_exponential(np.asarray(v))
    for k,v in mle_estimates.items():
        print(k,v)       
    
    fig = plt.figure()
    plt.gcf().subplots_adjust(bottom=0.15)
    '''
    ax1 = fig.add_subplot(211)
    '''
    data_plot = [[y for y in x if not np.isnan(y)] for x in all_models.values()]
    data_labels = [x for x in list(all_models.keys())]
    print(','.join(data_labels))
    doc = Document("multirow")
    section = Section('Multirow Test')
    test2 = Subsection('MultiRow')
    col_head = '|'.join(['c']+['p{.76cm}']*20+['p{1cm}']*5)
    table2 = Tabular(col_head,booktabs=True)
    table2.add_row(tuple(['']+ [x for x in data_labels]))
    for s in sit_distr.keys():
        if s == 'TASK':
            continue
        
        table2.add_hline()
        est_vect = []
        for m in data_labels:
            if sum(sit_distr[s][list(sit_distr[s].keys())[0]][m]) == 0 and len(sit_distr[s][list(sit_distr[s].keys())[0]][m]) > 0:
                est_vect.append(np.inf)
            elif sum(sit_distr[s][list(sit_distr[s].keys())[0]][m]) != 0:
                est_vect.append(round(len(sit_distr[s][list(sit_distr[s].keys())[0]][m])/sum(sit_distr[s][list(sit_distr[s].keys())[0]][m]),1))
            else:
                est_vect.append(np.nan)
        table2.add_row(tuple([s]+ ['']*25))
        for idx,sval in enumerate(list(sit_distr[s].keys())):
            if sval == 'HIGH  SPEED':
                continue
            if idx >= 0:
                est_vect = []
                for m in data_labels:
                    if sum(sit_distr[s][sval][m]) == 0 and len(sit_distr[s][sval][m]) > 0:
                        est_vect.append(np.inf)
                    elif sum(sit_distr[s][sval][m]) != 0:
                        est_vect.append(round(len(sit_distr[s][sval][m])/sum(sit_distr[s][sval][m]),1))
                    else:
                        est_vect.append(np.nan)
                max_idx = est_vect.index(max(est_vect))
                table2.add_row(tuple([sval]+[str(x) if i!= max_idx else pylatex.utils.bold(str(x)+'*') for i,x in enumerate(est_vect)]))
    test2.append(table2)
    section.append(test2)
    doc.append(section)
    doc.generate_pdf(clean_tex=False,compiler='pdfLaTeX')   
            
    '''      
    for idx,m in enumerate(list(all_models.keys())):
        plt.text(idx+1, 0.00165, str(round(mle_estimates[m],1)), rotation=90, va='center')
    bp = ax1.boxplot(data_plot,showfliers=False,patch_artist=True)
    colors = ['cyan']*10+ ['tan']*10+ ['pink']*5
    for patch, color in zip(bp['boxes'], colors):
        patch.set_facecolor(color)
    
    plt.setp(bp['medians'], color='black')
    
    ax1.set_xticklabels([])
    for tick in ax1.get_xticklabels():
        tick.set_rotation(45)
    '''
    ax2 = fig.add_subplot(111)
    for idx,m in enumerate(list(all_models.keys())):
        plt.text(idx+.75, 0.00165, str(round(mle_estimates[m],1)), rotation=90, va='center')
    bp = ax2.boxplot(data_plot,showfliers=False,patch_artist=True)
    colors = ['#377eb8']*10+ ['#ff7f00']*10+ ['#4daf4a']*5
    for patch, color in zip(bp['boxes'], colors):
        patch.set_facecolor(color)
    
    plt.setp(bp['medians'], color='black')
    
    ax2.set_xticklabels([x for x in data_labels],ha='right')
    for tick in ax2.get_xticklabels():
        tick.set_rotation(45)
    plt.show()
    
    
if __name__ == '__main__':     
    plot_util_hist()
    
